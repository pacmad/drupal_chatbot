(function ($, Drupal, drupalSettings) {
  var botLogo='';
  var botuserlogo = '';
  var accesstoken = '';
  Drupal.behaviors.DrupalbotBehavior = {
    attach: function (context, settings) {
      // Accessing Config Variables from setting from 'drupalSettings';
      var botLogo = drupalSettings.chatbot.chatbotdata.botlogo;
      var botuserlogo = drupalSettings.chatbot.chatbotdata.botuserlogo;
      var accesstoken = drupalSettings.chatbot.chatbotdata.access_token;

      $('#noaModal').modal('show');
      // $('#noaModal').modal('toggle');
   }
  };

  var botLogo = drupalSettings.chatbot.chatbotdata.botlogo;
  var botuserlogo = drupalSettings.chatbot.chatbotdata.botuserlogo;
  var accesstoken = drupalSettings.chatbot.chatbotdata.access_token;

  var local = {};
  local.avatar = botuserlogo;

  var remote = {};
  remote.avatar = botLogo;

  var accessToken = accesstoken,
  baseUrl = "https://api.api.ai/v1/",
  $speechInput,
  $recBtn,
  recognition,
  messageRecording = "Recording...",
  messageCouldntHear = "I couldn't hear you, could you say that again?",
  messageInternalError = "Oh no, there has been an internal server error",
  messageSorry = "I'm sorry, I don't have the answer to that yet.",
  defaultInput = "Escribir aquí...",
  defaultBotDelay = 1000,
  defaultDelay = 2000;


  function startRecognition() {
    recognition = new webkitSpeechRecognition();
    recognition.continuous = false;
    recognition.interimResults = false;

    recognition.onstart = function(event) {
      $speechInput.val(messageRecording);
      respond(messageRecording);
      updateRec();
    };
    recognition.onresult = function(event) {
      recognition.onend = null;

      var text = "";
      for (var i = event.resultIndex; i < event.results.length; ++i) {
        text += event.results[i][0].transcript;
      }

    	if (checkIfEmailInString(text)){
    		text = text.replace(/ /g, '');
    		text = text.toLowerCase();
    		text = text.trim();
    	}
      $speechInput.val(" ");
      setInput(text);
      stopRecognition();
    };
    recognition.onend = function() {
      respond(messageCouldntHear);
      stopRecognition();
    };
    recognition.lang = "en-US";
    recognition.start();
  }


  function stopRecognition() {
    if (recognition) {
      recognition.stop();
      recognition = null;
      $speechInput.attr('placeholder','Type a message or speak').val("").focus();
      $($recBtn).css("background", "#337ab7");
    }
    updateRec();
  }

  function switchRecognition() {
    if (recognition) {
      stopRecognition();
    } else {
      startRecognition();
    }
  }

  function setInput(text) {
    insertChat("local", text);
    queryBot(text);
  }

  function updateRec() {
    $recBtn.text(recognition ? "Stop" : "Speak");
  }




  function respond(val) {
    if (val == "") {
      val = messageSorry;
    }

    if (val !== messageRecording) {
      console.log($keyPress);
      var msg = new SpeechSynthesisUtterance();
      msg.voiceURI = "native";
      msg.text = val;
      msg.lang = "en-US";
      window.speechSynthesis.speak(msg);
    }

  }

  function formatTime(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12;
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
  }

  function checkIfEmailInString(text) {
    var re = /(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(".+\"))\s*@\s*((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/;
    return re.test(text);
  }


  // var botui = new BotUI('botui-app') // id of container
  /*
  botui.message.bot({ // show first message
    delay: 200,
    content: 'hello'
  }).then(() => {
    return botui.message.bot({ // second one
      delay: 1000, // wait 1 sec.
      content: 'how are you?'
    })
  });
  */



var botui = BotUI('botui-app');


/*
homeBot.message.add({
  content: 'Hello there!'
}).then(function () {
  return homeBot.message.add({
    delay: 1500,
    content: 'Wanna see a demo of what BotUI can do?'
  });
}).then(function () {
  return homeBot.action.button({
    delay: 1000,
    action: [{
      text: 'Sure!',
      value: 'sure'
    }, {
      text: 'Just skip to end 😒',
      value: 'skip'
    }]
  });
}).then(function (res) {
  if(res.value == 'sure') {
    tutorial();
  }
  if(res.value == 'skip') {
    end();
  }
});
*/






/*


  botui.message.bot({ // show first message
    delay: 200,
    content: 'hello'
  }).then(() => {
    return botui.message.bot({ // second one
      delay: 1000, // wait 1 sec.
      content: 'how are you?'
    })
  }).then(() => {
    return botui.action.button({ // let user do something
      delay: 1000,
      action: [
        {
          text: 'Good',
          value: 'good'
        },
        {
          text: 'Really Good',
          value: 'really_good'
        }
      ]
    })
  }).then(res => {
    return botui.message.bot({
      delay: 1000,
      content: `You are feeling ${res.text}!`
    })
  })



*/

/*
botui.message.add({
  content: 'Hi, <strong>how can I help you</strong> today?',
  type: 'html',
  delay: 1500,
}).then(function () {
  botui.action.text({
    action: {
      placeholder: 'Write here...',
    }
  }
).then(function (res) {
  socket.emit('fromClient', { client : res.value }); // sends the message typed to server
    console.log(res.value); // will print whatever was typed in the field.
  }).then(function () {
    socket.on('fromServer', function (data) { // recieveing a reply from server.
      console.log(data.server);
      newMessage(data.server);
      addAction();
  })
});
})
*/
function newMessage (response) {
  botui.message.add({
    content: response,
    delay: 0,
  })
}

function addAction () {
  botui.action.text({
    action: {
      placeholder: defaultInput,
    }
  }).then(function (res) {
    // socket.emit('fromClient', { client : res.value });
    // console.log('client response: ', res.value);
    queryBot(res.value);

  })
}


  function insertChat(who, text){

    var control = "";
    var date = formatTime(new Date());

    if (who == "local"){
      control = '<li style="width:100%;">' +
                      '<div class="msj-rta macro">' +
                          '<div class="text text-r">' +
                              '<p>'+text+'</p>' +
                              '<p><small>'+date+'</small></p>' +
                          '</div>' +
                      '<div class="avatar" style="padding:0px 0px 0px 10px !important"><img class="img-circle" style="width:100%;" src="'+local.avatar+'" /></div>' +
                '</li>';
      botui.message.add({
        human: true,
        delay: defaultDelay,
        content: text,
      })

    } else {
      control = '<li style="width:100%">' +
                      '<div class="msj macro">' +
                      '<div class="avatar"><img class="img-circle" style="width:100%;" src="'+ remote.avatar +'" /></div>' +
                          '<div class="text text-l">' +
                              '<p>'+ text +'</p>' +
                              '<p><small>'+date+'</small></p>' +
                          '</div>' +
                      '</div>' +
                  '</li>';
      botui.message.add({
        loading: true,
        delay: defaultBotDelay,
        content: text,
      }).then(function (res) {
        // console.log(res.value); // will print whatever was typed in the field.
        addAction();
      })
      /*
      .then(function () { // wait till its shown
        return botui.action.text({ // show 'text' action
          action: {
            placeholder: 'Your name'
          }
        });
      })
      */




    }
    /*
    $("#messages").append(control);
    var objDiv = document.getElementById("messages");
    objDiv.scrollTop = objDiv.scrollHeight;
    */
  }

  $("#chat-panel").on('click',function(){
      $(".innerframe").toggle();
  });

  function resetChat(){
      $("#messages").empty();
  }

  $(".mytext").on("keyup", function(e){
    if (e.which == 13){
  	$keyPress=e.which;
      var text = $(this).val();
      if (text !== ""){
        insertChat("local", text);
        $(this).val('');
        queryBot(text)
      }
    }
  });


  botui.message.add({
    // content: 'Hi, <strong>how can I help you</strong> today?',
    content: 'Hola, ¿<strong>cómo puedo ayudarte</strong> hoy?',
    type: 'html',
    delay: defaultDelay,
  }).then(function () {
    botui.action.text({
      action: {
        placeholder: defaultInput,
      }
    }).then(function (res) {
      firstqueryBot(res.value); // sends the message typed to server
      console.log(res.value); // will print whatever was typed in the field.
    })
  })
  function getIntro() {

    // firstqueryBot('Hi');

   }


  function firstqueryBot(text) {
    $.ajax({
      type: "POST",
      url: baseUrl + "query?v=20150910",
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      headers: {
          "Authorization": "Bearer " + accessToken
      },
      data: JSON.stringify({ query: text, lang: "en", sessionId: "1234567890" }),
      success: function(data) {
        var textResult = data.result.fulfillment.speech;
        var matches = textResult.match(/href="([^"]*)/)
        if (matches){
          matches = textResult.match(/href="([^"]*)/)[1];
          var lochref = matches;
          window.location.assign(lochref);
        }
        insertChat("remote",data.result.fulfillment.speech);

      }, error: function() {
        insertChat("remote","Internal Server Error! Sorry For Unavailability");
      }
    });
  }

  function queryBot(text) {
    // $('.botui-app-container').data('state','waiting');
    $('.bg-modal-content').attr('data-state', 'waiting');

    $('.botui-app-container').addClass('waiting');
    $.ajax({
      type: "POST",
      url: baseUrl + "query?v=20150910",
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      headers: {
          "Authorization": "Bearer " + accessToken
      },
      data: JSON.stringify({ query: text, lang: "en", sessionId: "1234567890" }),
      success: function(data) {
        var textResult = data.result.fulfillment.speech;
        var matches = textResult.match(/href="([^"]*)/)
        if (matches){
          matches = textResult.match(/href="([^"]*)/)[1];
          var lochref = matches;
          window.location.assign(lochref);
        }
        insertChat("remote",data.result.fulfillment.speech);

        if(!$keyPress){
          respond(data.result.fulfillment.speech);
        }
      }, error: function() {
        insertChat("remote","Internal Server Error");
      }
    });
  }



var $target = $('.bg-modal-content'),
    timerId,
    currentFrame = 0;

$target.hover(function(){
  timerId = setInterval(function(){
  $target.removeClass('frame' + currentFrame);
  currentFrame++;

  if(currentFrame > 11 ) { clearInterval(timerId);currentFrame = 0;};

  $target.addClass('frame'+ currentFrame);
 },220);
}, function(){
  clearInterval(timerId);
});


$(document).ready(function() {

  if(sessionStorage.getItem('messageContent')) {
		$('#messages').html(sessionStorage.getItem('messageContent'));
	} else{
		getIntro();
	}
  $speechInput = $("#speech");
  $recBtn = $("#rec");
  $keyPress = 0;
  $recBtn.on("click", function(event) {
    $keyPress = 0;
	  $(this).css("background", "#faa635");
      switchRecognition();
    });
  });
  /*
  $(window).bind('beforeunload', function() {
    // save info somewhere
    var messageContent='';
    messageContent = document.getElementById("messages").innerHTML;
    sessionStorage.setItem('messageContent', messageContent);
	});
  */



}(jQuery, Drupal, drupalSettings));


